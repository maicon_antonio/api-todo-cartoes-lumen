<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Fabrica extends Model
{
    protected $table = 'tbl_fabrica';
    protected $primaryKey = 'fabrica';
    public $timestamps = false;

    protected $fillable = [
       "fabrica"
    ];

}
