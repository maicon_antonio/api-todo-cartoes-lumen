<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CartaoServico extends Model
{
    protected $table = 'tbl_cartao_servico';
    protected $primaryKey = 'cartao_servico';
    public $timestamps = false;

    protected $fillable = [
       "cartao_servico"
    ];

}
