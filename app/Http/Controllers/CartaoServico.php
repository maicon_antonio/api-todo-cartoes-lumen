<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Tc\Regras\RulesLoader,
    App\Tc\Regras\RuleEngine\Queue,
    App\Tc\Regras\RuleEngine\Response;
use App\Helpers\Funcoes;


class CartaoServico extends BaseController
{
    use Funcoes;
    
    public function post(Request $request)
    {

        $data = $request->all();

        $response = $this->validaAcesso($request->headers->get('access-application-key'));

        $identifier = $response;

        $loader = new RulesLoader($identifier, 'gravar/cartao');
        $queue = new Queue($loader->getRules());

        return $queue->run($identifier, $data);

    }
}
