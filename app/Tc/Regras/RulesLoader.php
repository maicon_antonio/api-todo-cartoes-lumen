<?php

namespace App\Tc\Regras;

class RulesLoader
{
    private $rules;

    public function __construct($identifier, $rule)
    {
        $this->rules = $this->loadRules($identifier, $rule);
    }

    public function loadRules($identifier, $rule)
    {
        if (file_exists(__DIR__ . '/./config/' . $rule . '/' . $identifier . '.json')) {
            $rules = json_decode(file_get_contents(__DIR__ . '/./config/' . $rule . '/' . $identifier . '.json'), true);
        } else {
            $rules = json_decode(file_get_contents(__DIR__ . '/./config/' . $rule . '/default.json'), true);
        }

        return $rules;
    }

    public function getRules()
    {
        return $this->rules;
    }
}
