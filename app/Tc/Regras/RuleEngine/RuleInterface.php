<?php

namespace App\Tc\Regras\RuleEngine;


interface RuleInterface
{
    public function process(string $identifier, array $data, $next = null);
}
