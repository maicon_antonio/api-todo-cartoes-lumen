<?php

namespace App\Tc\Regras\RuleEngine;

use App\Tc\Regras\RuleEngine\Response;


class Queue
{

    private $rules;

    public function __construct($rules) {
        $this->rules = $rules;
    }

    public function run(string $identifier, array $data) {
        $rule = $this->getNext();

        if (!empty($rule)) {
            return $rule->process($identifier, $data, $this);
        }

        $responseCode = 200;

        if (array_key_exists('http_response_code', $data)) {
            $responseCode = $data['http_response_code'];
        }

        if (array_key_exists('request_method', $data)) {
            $requestMethod = $data['request_method'];
        }

        $response = new Response($data, $responseCode);

        return $response->response();
    }

    public function process(string $identifier, array $data) {
        return $this->run($identifier, $data);
    }

    private function getNext() {
        $next = array_shift($this->rules);
        if (!empty($next)) {
            return new $next['name'];
        }

        return false;
    }
}
