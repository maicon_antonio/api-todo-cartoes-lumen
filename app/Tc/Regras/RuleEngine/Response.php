<?php

namespace App\Tc\Regras\RuleEngine;


class Response
{
    private $statusCode;
    private $requestMethod;
    private $return;

    public function __construct($content, $statusCode = 200, $requestMethod = null)
    {
        $this->statusCode = $statusCode;
        $this->requestMethod = strtolower($_SERVER['REQUEST_METHOD']);

        if (!empty($requestMethod)) {
            $this->requestMethod = strtolower($requestMethod);
        }

        return $this->build($content);
    }

    private function build($content)
    {
        $toStr = (string) $this->statusCode;

        if ($toStr[0] == '2') {
            return $this->returnSuccess($content);
        }

        if (in_array($toStr[0], ['4', '5'])) {
            return $this->returnError($content);
        }
    }

    private function returnSuccess($content)
    {
        switch ($this->requestMethod) {
            case 'get':
                return $this->returnSuccessGet($content);
                break;
            case 'post':
                return $this->returnSuccessPost($content);
                break;
            default:
                return $this->returnError(500);
        }
    }

    private function returnSuccessGet($content)
    {
        $preCond = [
            'data',
            'results',
            'pages',
            'perPage',
            'current',
            'type',
            'id',
            'links'
        ];

        foreach ($preCond as $cond) {
            if (!array_key_exists($cond, $content)) {
                $this->statusCode = 500;

                return $this->returnError($cond);
            }
        }

        $this->return = [
            'meta' => [
                'result' => 'success',
                'status' => $this->statusCode,
                'totalResults' => $content['results'],
                'totalPerPage' => $content['perPage'],
                'totalPages' => $content['pages'],
                'currentPage' => $content['current']
            ],
            'data' => [
                'type' => $content['type'],
                'id' => $content['id'],
                'attributes' => $content['data']
            ],
            'links' => $content['links']
        ];
    }

    private function returnSuccessPost($content)
    {
        $this->return = $content;
    }

    private function returnError($error)
    {
        switch ($error) {
            case (is_string($error)):
                $ret = [
                    'status' => $this->statusCode,
                    'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                    'title' => 'Erro interno',
                    'detail' => $error
                ];

                break;
            case (is_array($error)):
                $preCond = [
                    'source',
                    'title',
                    'detail'
                ];

                foreach ($preCond as $cond) {
                    if (!array_key_exists($cond, $error)) {
                        $this->statusCode = 500;

                        return $this->returnError($cond);
                    }
                }

                $ret = [
                    'status' => $this->statusCode,
                    'source' => $error['source'],
                    'title' => $error['title'],
                    'detail' => $error['detail']
                ];

                break;
            default:
                $ret = [
                    'status' => 500,
                    'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                    'title' => 'Erro interno',
                    'detail' => 'Função returnError'
                ];
        }


        $this->return = [
            'errors' => [
                0 => $ret
            ]
        ];
    }

    public function response()
    {
        return response()->json($this->return, $this->statusCode);
    }

}
