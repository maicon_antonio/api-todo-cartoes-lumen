<?php

namespace App\Tc\Posvenda\Cartao;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;


class ValidarObrigatorios implements RuleInterface
{

	private static $camposObrigatorios = [
		['key' => 'numero_cartao', 'type' => 'string', 'attributes' => ['max_length' => 15]],
		['key' => 'cnpj_intermediador', 'type' => 'numeric', 'attributes' => ['max_length' => 14]],
		['key' => 'cnpj_revenda', 'type' => 'numeric', 'attributes' => ['max_length' => 14]],
		['key' => 'valor_total', 'type' => 'float'],
		['key' => 'valor_comissao_venda', 'type' => 'float'],
		['key' => 'data_criacao', 'type' => 'date', 'attributes' => ['format' => 'DD-MM-YYYY']],
		['key' => 'codigo_transacao', 'type' => 'string', 'attributes' => ['max_length' => 8]],
	];

    public function process(string $identifier, array $data, $next = null)
    {

    	$obrigatorios = self::$camposObrigatorios;

		$data['data'] = [
			'fabrica' => $identifier,
			'campos_obrigatorios' => $obrigatorios
		];

		$data['results'] = count($obrigatorios);
		$data['pages'] = 1;
		$data['perPage'] = count($obrigatorios);
		$data['current'] = 1;

		$data['type'] = 'campos_obrigatorios';
		$data['id'] = $identifier;

		return $next->process($identifier, $data);

    }

    public static function getCamposObrigatorios()
    {
        return self::$camposObrigatorios;
    }

    public static function deParaCampos(array $data)
    {

    	$deparaCampos = [
    		"store_cnpj" => "cnpj_intermediador",
    		"card_number" => "numero_cartao",
    		"total" => "valor_total",
    		"sales_commission" => "valor_comissao_venda",
    		"CNPJ" => "cnpj_revenda",
    		"order_created_at" => "data_criacao",
    		"transaction_code" => "codigo_transacao"
    	];

    	foreach ($deparaCampos as $key => $value) {
    		if (!empty($data[$key])) {
    			$data[$value] = $data[$key];
    			unset($data[$key]);
    		}
    	}

    	return $data;

    }

}

?>