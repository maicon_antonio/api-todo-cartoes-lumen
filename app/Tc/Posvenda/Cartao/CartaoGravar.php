<?php

namespace App\Tc\Posvenda\Cartao;

use App\Tc\Regras\RuleEngine\RuleInterface,
    App\Tc\Regras\RuleEngine\Response;
use App\Models\CartaoServico;
use App\Tc\Posvenda\Cartao\ValidarObrigatorios as oCamposObrigatorios;


class CartaoGravar implements RuleInterface
{
    public function process(string $identifier, array $data, $next = null)
    {

    	$obrigatorios = oCamposObrigatorios::getCamposObrigatorios();
    	$data = oCamposObrigatorios::deParaCampos($data);

    	foreach ($obrigatorios as $obr) {
            $key = $obr['key'];
            $type = $obr['type'];
            $attr = (array_key_exists('attributes', $obr)) ? $obr['attributes'] : [];

            if (!array_key_exists($key, $data) || empty($data[$key])) {
                $error = [
                    'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                    'title' => 'Campo obrigatório',
                    'detail' => $obr
                ];

                $response = new Response($error, 406);

                return $response->response();
            }

            if (in_array($type, ['string', 'numeric']) && array_key_exists('max_length', $attr)) {
                if (strlen($data[$key]) > $attr['max_length']) {
                    $error = [
                        'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                        'title' => 'Tamanho máximo de campo excedido',
                        'detail' => $obr
                    ];

                    $response = new Response($error, 406);

                    return $response->response();
                }
            }

            if ($type == 'date' && array_key_exists('format', $attr)) {

            	$novaData = date("Y-m-d", strtotime($data[$key]));

                $src = ['DD', 'MM', 'YYYY'];
                $rep = ['Y', 'm', 'd'];
                $format = str_replace($src, $rep, $attr['format']);

                $date = \DateTime::createFromFormat($format, $novaData);

                if (empty($date) || ($date->format($format) != $novaData)) {
                	$error = [
                        'source' => ['pointer' => $_SERVER['REQUEST_URI']],
                        'title' => 'Data inválida',
                        'detail' => $obr
                    ];

                    $response = new Response($error, 406);

                    return $response->response();
                }

                $data[$key] = $novaData;
            }
        }

        $oCartaoServico = new CartaoServico();

        $save = $data;
		unset($save['data']);
		unset($save['results']);
		unset($save['pages']);
		unset($save['perPage']);
		unset($save['current']);
		unset($save['type']);
		unset($save['id']);
		$save['fabrica'] = $identifier;

        foreach ($save as $key => $val) {
            $oCartaoServico->$key = $val;
        }

        $oCartaoServico->save();

		$content = [
			'meta' => [
				'result' => 'sucess',
				'status'=> 201
			],
			'data' => [
				'type' => 'cartao_servico',
				'id' => $oCartaoServico->cartao_servico
			]/*,
			'links' => [
				'self' => '/api-os/os/' . $os->os
			]*/
		];

        return $next->process($identifier, $data);
    }
}

?>